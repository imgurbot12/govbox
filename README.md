# GoVBox
---
generated bindings from [govbox-gen](https://gitlab.com/imgurbot12/govboxcom)

allows direct communication with the virtualbox management system
via their XPCOM API. These are complete bindings, every exposed function
is available and supported to be called from golang.

all code is aviable via the release/* branches. master will not be touched.
thanks! - imgurbot12
