#!/bin/bash

#** Variables **#
txtred="\033[31m"
txtbld="\033[1m"
txtrst="\033[0m"

#** Functions **#

setup() {
  # check if virtualbox is installed via vboxmange utility
  if ! command -v vboxmanage >/dev/null; then
    echo -e "$txtred[ERROR]$txrst virtualbox not installed. cannot install!"
    exit 1
  fi
  if ! command -v go 2>&1 >/dev/null; then
    echo -e "$txtred[ERROR]$txrst golang not installed. cannot install!"
    exit 1
  fi
  # check if GOPATH exists
  if ! ls $GOPATH 2>&1 >/dev/null; then
    echo -e "$txtred[ERROR]$txtrst GOPATH not accessable, please update GOPATH."
  fi
  # source go environment
  eval $(go env | awk '{print "export " $0 ";"}')
}

get_branch() {
  # get git-branch that matches major.minor version of virtualbox installed
  major_minor=$(vboxmanage --version | grep -Eoh '[0-9]+\.[0-9]+')
  echo $(
    git ls-remote 2>&1 | \
    awk '/release/ {print $2}' | \
    awk -F'heads/' "/$major_minor/ {print \$2}"
  )
}

copy_resources() {
  mkdir -p $INSTALL
  rm -rf $INSTALL/*
  cp *.go  $INSTALL
  cp -r c/ $INSTALL
}

make_libraries() {
  back=$(pwd)
  cd $INSTALL/c/
  make > /dev/null
  cd $back
}

link_dynamic_library() {
  # attempt to get root
  if [ "$EUID" -ne 0 ]; then
    echo -e "$txtbld[INFO]$txtrst linked-libraries require placement in root folders"
    if ! sudo -v; then
      echo -e "$txtred[ERROR]$txtrst unable to obtain root access"
      exit 1
    fi
  fi
  # place dynamic library in new folder
  sudo mv $INSTALL/c/libvbox.so /usr/lib/.
}

#** Start **#

setup

INSTALL=$GOPATH/src/gitlab.com/imgurbot12/govbox

# find the best release branch for the installed version of virtualbox
branch=$(get_branch)
if [ "$branch" == "" ]; then
  echo -e "$txtred[ERROR]$txtrst unable to find branch w/ major.minor=$major_minor"
  exit 1
else
  echo -e "$txtbld[INFO]$txtrst installing version $branch"
fi

# switch to required branch for install
if ! git checkout $branch; then
  echo -e "$txtred[ERROR]$txtrst unable to switch branches"
  exit 1
fi

# checkout the requested branch and move the repo into GOPATH
echo -e "$txtbld[INFO]$txtrst copying src files into GOPATH"
copy_resources

echo -e "$txtbld[INFO]$txtrst making c-libraries"
make_libraries

echo -e "$txtbld[INFO]$txtrst linking dynamic libraries"
link_dynamic_library

# switch back to master
git checkout master
echo -e "$txtbld[INFO]$txtrst install complete"
